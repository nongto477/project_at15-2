<#macro registrationLayout bodyClass="" layoutClass="" displayInfo=false displayMessage=true displayRequiredFields=false showAnotherWayIfPresent=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">
	<#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
	<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--<link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />-->
    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
	<title>${msg("loginTitle",(realm.displayName!''))}</title>
</head>

<body>
	<div class="main row">
        <div class="box-left col-md-6">
            <div class="logo">
                <img src="${url.resourcesPath}/img/Logo.png" alt="" srcset="">
            </div>
            <div class="car">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators carousel-indicators-custom">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img class="d-block w-100" src="${url.resourcesPath}/img/carousel/image-1.png" alt="First slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="${url.resourcesPath}/img/carousel/image-1.png" alt="Second slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="${url.resourcesPath}/img/carousel/image-1.png" alt="Third slide">
                      </div>
                    </div>
				</div>
            </div>
        </div>
        <div class="box-right col-md-6">
            <div class="${layoutClass}">
				<#nested "header">
				<#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
					<div class="alert-${message.type} ${properties.kcAlertClass!} pf-m-<#if message.type = 'error'>danger<#else>${message.type}</#if>">
						<div class="pf-c-alert__icon">
							<#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
							<#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
							<#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
							<#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
						</div>
							<span class="${properties.kcAlertTitleClass!}">${kcSanitize(message.summary)?no_esc}</span>
					</div>
				</#if>
                <#nested "form">
				<#if displayInfo>
                	<#nested "info">
				</#if>
				<#nested "social">
            </div>
        </div>
    </div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<#if properties.scriptsCommon?has_content>
        <#list properties.scriptsCommon?split(' ') as script>
			<script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
</body>
</html>
</#macro>
