<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login form-register" displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled?? displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#if section = "header">
        <h3 class="headline">
            ${msg("registerTitle")}
        </h3>
        <p class="text-register">
            Manager all your information
        </p>
        <p class="text-description">You can verify your personal account and begin setting up your profile</p>
    <#elseif section = "form">
        <form id="kc-register-form" action="${url.registrationAction}" method="post">
            <div class="row">
                <div class="form-group col-md-6">
                    <input class="form-control form-input" type="text" id="firstName" name="firstName" value="${(register.formData.firstName!'')}"
                    aria-invalid="<#if messagesPerField.existsError('firstName')>true</#if>" placeholder="${msg('firstName')}">

                    <#if messagesPerField.existsError('firstName')>
                        <span id="input-error-firstname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('firstName'))?no_esc}
                        </span>
                    </#if>
                </div>

                <div class="form-group col-md-6">
                    <input class="form-control form-input" type="text" id="lastName" name="lastName" value="${(register.formData.lastName!'')}"
                    aria-invalid="<#if messagesPerField.existsError('lastName')>true</#if>" placeholder="${msg('lastName')}">

                    <#if messagesPerField.existsError('lastName')>
                        <span id="input-error-lastName" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('lastName'))?no_esc}
                        </span>
                    </#if>
                </div>

                <div class="form-group col-md-6">
                    <input class="form-control form-input" type="text" id="email" name="email" value="${(register.formData.email!'')}"
                    aria-invalid="<#if messagesPerField.existsError('email')>true</#if>" placeholder="${msg('email')}">

                    <#if messagesPerField.existsError('email')>
                        <span id="input-error-email" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('email'))?no_esc}
                        </span>
                    </#if>
                </div>

                <#if !realm.registrationEmailAsUsername>
                    <div class="form-group col-md-6">
                        <input class="form-control form-input" type="text" id="username" name="username" value="${(register.formData.username!'')}"
                        aria-invalid="<#if messagesPerField.existsError('username')>true</#if>" placeholder="${msg('username')}">

                        <#if messagesPerField.existsError('username')>
                            <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('username'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </#if>

                <#if passwordRequired??>
                    <div class="form-group col-md-6">
                        <input class="form-control form-input" type="password" id="password" name="password" value="${(register.formData.password!'')}"
                        aria-invalid="<#if messagesPerField.existsError('password')>true</#if>" placeholder="${msg('password')}">

                        <#if messagesPerField.existsError('password')>
                            <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password'))?no_esc}
                            </span>
                        </#if>
                    </div>

                    <div class="form-group col-md-6">
                        <input class="form-control form-input" type="password" id="password-confirm" name="password-confirm"
                        aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>" placeholder="${msg('passwordConfirm')}">

                        <#if messagesPerField.existsError('password-confirm')>
                            <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </#if>

                <#if recaptchaRequired??>
                    <div class="form-group col-md-6">
                        <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                    </div>
                </#if>
            </div>
            <#--  <div class="form-group form-checkbox">
                <label for="vehicle1">
                    Yes, I agree your <span>terms and conditions</span>
                    <input type="checkbox" id="vehicle1" name="vehicle1" value="">
                </label>
            </div>  -->
            <div class="form-group form-checkbox">
                <label for="vehicle1">
                    <input type="checkbox" id="vehicle2" name="vehicle2" value="">
                    Yes, I want to receive Magicak emails
                </label>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-login btn-register">${msg("doRegister")}</button>
                </div>
            </div>
        </form>
    <#elseif section = "info">
        <p class="text-account">${msg("haveAccount")} <a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></p>
    <#elseif section = "social">
        <#if realm.password && social.providers??>
            <div class="text-or register">
                <span>OR</span>
                <div class="line"></div>
            </div>
            <div class="register-other">
                <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountListGridClass!}</#if>">
                    <#list social.providers as p>
                        <a id="social-${p.alias}" class="${properties.kcFormSocialAccountListButtonClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                type="button" href="${p.loginUrl}">
                            <#if p.iconClasses?has_content>
                                <i class="${properties.kcCommonLogoIdP!} ${p.iconClasses!}" aria-hidden="true"></i>
                                <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName!}</span>
                            <#else>
                                <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName!}</span>
                            </#if>
                        </a>
                    </#list>
                </ul>
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>