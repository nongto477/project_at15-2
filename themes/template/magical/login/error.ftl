<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login" displayMessage=false; section>
    <#if section = "header">
        <h3 class="headline">
            ${msg("errorTitle")}
        </h3>
    <#elseif section = "form">
        <div id="kc-error-message">
            <p class="instruction">${message.summary?no_esc}</p>
            <#if client?? && client.baseUrl?has_content>
                <p><a id="backToApplication" href="${client.baseUrl}">${kcSanitize(msg("backToApplication"))?no_esc}</a></p>
            </#if>
        </div>
    </#if>
</@layout.registrationLayout>