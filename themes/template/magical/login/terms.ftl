<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login page-term" displayMessage=false; section>
    <#if section = "header">
        <h3 class="headline">
            ${msg("termsTitle")}
        </h3>
    <#elseif section = "form">
        <div class="content">
            ${kcSanitize(msg("termsText"))?no_esc}
        </div>
        <form action="${url.loginAction}" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <input class="btn btn-login btn-register" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}"/>
                </div>
                <div class="col-md-6">
                    <input class="btn btn-login btn-register" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
