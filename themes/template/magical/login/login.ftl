<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login" displayMessage=!messagesPerField.existsError('username','password') displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled??; section>
    <#if section = "header">
        <h3 class="headline">
            ${msg("loginAccountTitle")}
        </h3>
    <#elseif section = "form">
        <#if realm.password>
            <form id="kc-form-login" action="${url.loginAction}" onsubmit="login.disabled = true; return true;" method="post">
                <div class="form-group">
                    <#if usernameEditDisabled??>
                        <input tabindex="1" class="form-control form-input" id="username" name="username" value="${(login.username!'')}" type="text" 
                            placeholder="<#if !realm.loginWithEmailAllowed>${msg('username')}<#elseif !realm.registrationEmailAsUsername>${msg('usernameOrEmail')}<#else>${msg('email')}</#if>" disabled/>
                    <#else>
                        <input tabindex="1" class="form-control form-input" id="username" name="username" value="${(login.username!'')}" type="text" 
                            placeholder="<#if !realm.loginWithEmailAllowed>${msg('username')}<#elseif !realm.registrationEmailAsUsername>${msg('usernameOrEmail')}<#else>${msg('email')}</#if>" autofocus autocomplete="off"
							aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"/>

                        <#if messagesPerField.existsError('username','password')>
                            <span id="input-error" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                    ${kcSanitize(messagesPerField.getFirstError('username','password'))?no_esc}
                            </span>
                        </#if>
                    </#if>
                </div>
                <div class="form-group form-password">
                    <input tabindex="2" class="form-control form-input" id="password" name="password" type="password" autocomplete="off" 
                        placeholder="${msg('password')}" aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"/>
                    <div class="eyes">
                        <img src="${url.resourcesPath}/img/icon/Hide.png" alt="" srcset="">
                    </div>
                </div>
                <div class="row">
                    <#if realm.rememberMe && !usernameEditDisabled??>
                        <div class="form-check col-6 col-md-6">
                            <label class="form-check-label text-label">
                                <#if login.rememberMe??>
                                    <input tabindex="3" class="form-check-input" id="rememberMe" name="rememberMe" type="checkbox" checked> ${msg("rememberMe")}
                                <#else>
                                    <input tabindex="3" class="form-check-input" id="rememberMe" name="rememberMe" type="checkbox"> ${msg("rememberMe")}
                                </#if>
                            </label>
                        </div>
                    </#if>
                    <#if realm.resetPasswordAllowed>
                        <div class="text-forgot col-6 col-md-6 text-right">
                            <span><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                        </div>
                    </#if>
                </div>
                <button type="submit" class="btn btn-login">${msg("loginAccountTitle")}</button>
            </form>
        </#if>
    <#elseif section = "info">
        <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
            <p class="text-account">${msg("noAccount")} <a href="${url.registrationUrl}">${msg("doRegister")}</a></p>
        </#if>
    <#elseif section = "social">
        <#if realm.password && social.providers??>
            <div class="text-or">
                <div class="line"></div>
                <span>OR</span>
                <div class="line"></div>
            </div>
            <div class="login-other text-center">
                <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountListGridClass!}</#if>">
                    <#list social.providers as p>
                        <a id="social-${p.alias}" class="${properties.kcFormSocialAccountListButtonClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                type="button" href="${p.loginUrl}">
                            <#if p.iconClasses?has_content>
                                <i class="${properties.kcCommonLogoIdP!} ${p.iconClasses!}" aria-hidden="true"></i>
                                <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName!}</span>
                            <#else>
                                <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName!}</span>
                            </#if>
                        </a>
                    </#list>
                </ul>
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>
