<#import "template.ftl" as layout>
<@layout.registrationLayout layoutClass="form-login form-forgot-password" displayInfo=true; section>
    <#if section = "header">
		<h3 class="headline">
			${msg("emailVerifyTitle")}
		</h3>
    <#elseif section = "form">
        <p class="instruction">${msg("emailVerifyInstruction1")}</p>
    <#elseif section = "info">
        <p class="instruction">
            ${msg("emailVerifyInstruction2")}
            <br/>
            <a href="${url.loginAction}">${msg("doClickHere")}</a> ${msg("emailVerifyInstruction3")}
        </p>
    </#if>
</@layout.registrationLayout>