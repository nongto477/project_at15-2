$(document).ready(function () {
    $('.carousel').carousel({
        interval: 6000
    });
    var eyes = $('*.eyes');
    eyes.click(function(){
        eyes.siblings('input').attr('type',function(_, attr){ return attr=="text" ? 'password' : 'text';});
    });
});