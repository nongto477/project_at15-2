module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/base'],
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'off' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    semi: ['error', 'always'],
    'max-len': 'off',
    'linebreak-style': 'off',
    camelcase: 'off',
    'arrow-parens': ['error', 'as-needed'],
    'vue/multiline-html-element-content-newline': 'off',
  },
}
