import Vue from 'vue';

// import { ToastPlugin, ModalPlugin } from 'bootstrap-vue';
import BootstrapVue from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import VueCompositionAPI from '@vue/composition-api';
import VueKeycloakJs from '@dsb-norge/vue-keycloak-js';
import axiosIns from '@/libs/axios';
import router from './router';
import store from './store';
import App from './App.vue';



// 3rd party plugins
import '@/libs/portal-vue';
import '@/libs/toastification';
import '@/libs/vue-select';

// // Axios Mock Adapter
// import '@/@fake-db/db';

// BSV Plugin Registration
// Vue.use(ToastPlugin);
// Vue.use(ModalPlugin);
Vue.use(BootstrapVue);

Vue.use(VueSweetalert2);
// Composition API
Vue.use(VueCompositionAPI);


function tokenInterceptor() {
  axiosIns.interceptors.request.use(config => {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${Vue.prototype.$keycloak.token}`;
    return config;
  }, error => Promise.reject(error));
}

function getSettings() {
  const settings = process.env.VUE_APP_API_ROOT + '/' + process.env.VUE_APP_API_VERSION + '/settings';
  try {
    const data = new Promise((resolve, reject) => {
      axiosIns
        .get(`${settings}`)
        .then(response => store.commit("app/SETTINGS", response.data.response))
        .catch(error => reject(error));
    });
  } catch (error) {
    console.log(error);
  }
}

Vue.use(VueKeycloakJs, {
  init: {
    onLoad: 'check-sso',
    silentCheckSsoRedirectUri: `${window.location.origin}/silent-check-sso.html`,
  },
  config: {
    url: process.env.VUE_APP_KEYCLOAK_URL,
    clientId: process.env.VUE_APP_KEYCLOAK_CLIENT_ID,
    realm: process.env.VUE_APP_KEYCLOAK_REALM,
  },
  onReady() {
    tokenInterceptor();
    // getSettings();
  },
});

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'sweetalert2/dist/sweetalert2.min.css';
// import assets styles
import './assets/css/main.css';


Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
