import requestApi from '@Service/RequestApiMiddleware';

const addToCart   = '/add-to-cart';
const cart        = '/cart';
const vnpay       = '/vnpay';

export default {
  addToCart(data) {
    return requestApi.post(`${addToCart}`,data);
  },
  getCart(){
    return requestApi.get(`${cart}`);
  },
  gotoVNpay(){
    return requestApi.get(`${vnpay}`);
  }
};
