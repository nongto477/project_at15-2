import requestApi from '@Service/RequestApiMiddleware';

const orders = '/orders';
export default {
  getOrder() {
    return requestApi.get(`${orders}`);
  },
};
