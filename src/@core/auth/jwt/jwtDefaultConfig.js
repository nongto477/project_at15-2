export default {
  // Endpoints
  // loginEndpoint: '/jwt/login',
  // loginEndpoint: 'https://magicak.com/auth/realms/magicakweb/protocol/openid-connect/token',
  loginEndpoint: 'https://auth.magicak.com/auth/realms/magicakweb/protocol/openid-connect/token',
  registerEndpoint: '/jwt/register',
  refreshEndpoint: '/jwt/refresh-token',
  logoutEndpoint: '/jwt/logout',

  // This will be prefixed in authorization header with token
  // e.g. Authorization: Bearer <token>
  tokenType: 'Bearer',

  // Value of this property will be used as key to store JWT token in storage
  storageTokenKeyName: 'accessToken',
  storageRefreshTokenKeyName: 'refreshToken',
};
