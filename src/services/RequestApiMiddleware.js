import axios from 'axios';
import Vue from 'vue';

export default axios.create({
  baseURL: 'https://shop-backend.thanhtienkr.dev' + '/api/' + process.env.VUE_APP_API_VERSION,
  headers: {
    Authorization: `Bearer ${Vue.prototype.$keycloak.token}`,
    'Content-Type': 'application/json, multipart/form-data',
    'Access-Control-Allow-Origin': '*',
    Accept: 'application/json'
  }
});
