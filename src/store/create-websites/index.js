export default {
    namespaced: true,
    state: {
        businessName: "",
        categoryID: 0,
        businessList: [],
        templateID: [],
        hostingPlan: [],
        domainUser: "",
        domainType: 0,
        locationList: [],
        location: "",
        authKey: "",
        currentStep: "",
        totalPrice: 0,
        totalPriceWithGST: 0,
    },
    getters: {},
    mutations: {
        UPDATE_BUSINESS_NAME(state, val) {
            state.businessName = val;
        },
        UPDATE_CATEGORY_ID(state, val) {
            state.categoryID = val;
        },
        UPDATE_TEMPLATE_ID(state, val) {
            state.templateID = val;
        },
        UPDATE_LOCATION(state, [val, location]) {
            state.locationList = val;
            state.location = location;
        },
        UPDATE_BUSINESS_LIST(state, val) {
            state.businessList = val;
        },
        UPDATE_HOSTING_PLAN(state, val) {
            state.hostingPlan = val;
        },
        UPDATE_DOMAIN_USER(state, val) {
            state.domainUser = val;
        },
        UPDATE_DOMAIN_TYPE(state, val) {
            state.domainType = val;
        },
        UPDATE_AUTH_KEY(state, val) {
            state.authKey = val;
        },
        CURRENT_STEP(state, val) {
            state.currentStep = val;
        },
        UPDATE_TOTAL_PRICE(state, val) {
            state.totalPrice = val;
        },
        UPDATE_TOTAL_PRICE_WITH_GST(state, val) {
            state.totalPriceWithGST = val;
        },
    },
    actions: {},
};
